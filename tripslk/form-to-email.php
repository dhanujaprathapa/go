<?php
if(!isset($_POST['submit']))
{
	//This page should not be accessed directly. Need to submit the form.
	echo "error; you need to submit the form!";
}
$name = $_POST['name'];
$lname = $_POST['last_name'];
$visitor_email = $_POST['email'];
$location = $_POST['country'];
$date_a = $_POST['date_a'];
$date_d= $_POST['date_d'];
$noAdult = $_POST['adults'];
$noChilds = $_POST['childrens'];
$message = $_POST['message'];
$places = $_POST['multi'];
//Validate first
if(empty($name)||empty($visitor_email))
{
    echo "Name and email are mandatory!";
    exit;
}

if(IsInjected($visitor_email))
{
    echo "Bad email value!";
    exit;
}

$email_from = "plan@tripslk.com";//<== update the email address
$email_subject = "$name's Trip Plan Request";
$email_body = "You have received a new plan request from $name $lname in $location\nDate of Arrival: $date_a\nDate of Departure: $date_d\nNo of Adults: $noAdult & No of Children: $noChilds\n".
    "Message:\n$message \n$places";

$to = "rasindeyiyo@yahoo.com";//<== update the email address
$headers = "From: $email_from \r\n";
$headers .= "Reply-To: $visitor_email \r\n";
//Send the email!
mail($to,$email_subject,$email_body,$headers);
//done. redirect to thank-you page.
header('Location: thank-you.html');


// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}

?>
